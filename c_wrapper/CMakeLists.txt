project(c_wrapper C)
cmake_minimum_required(VERSION 3.0)
find_package(PkgConfig REQUIRED)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c99")

set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -Wall")

IF(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
    # Set install directory to ../install
    set(CMAKE_INSTALL_PREFIX "${CMAKE_CURRENT_BINARY_DIR}/install" CACHE PATH "CMAKE_INSTALL_PREFIX: Install path prefix, prepended onto install directories." FORCE)
endif(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)

option(BUILD_WITH_ZMQ "Build with ZeroMQ" ON)

if(BUILD_WITH_ZMQ)
    mark_as_advanced(FORCE ZMQ_LIBRARY
        ZMQ_INCLUDE_DIR)
    find_path(ZMQ_DIR NAMES include/zmq.h)
    find_path(ZMQ_INCLUDE_DIR NAMES zmq.h HINTS ${ZMQ_DIR}/include)
    find_library(ZMQ_LIBRARY NAMES zmq libzmq HINTS ${ZMQ_DIR}/lib)
    set(ZMQ_INCLUDE_DIRS ${ZMQ_INCLUDE_DIR})
    include_directories( ${ZMQ_INCLUDE_DIR} )
    if(ZMQ_LIBRARY)
        set(EXTRA_LIBS ${EXTRA_LIBS}
            ${ZMQ_LIBRARY} )
        set( ENV{PKG_CONFIG_PATH} "$ENV{PKG_CONFIG_PATH}:${ZMQ_DIR}/lib/pkgconfig" )
        pkg_check_modules(ZMQ REQUIRED libzmq>=4.1.5)
    endif(ZMQ_LIBRARY)
    if(ZMQ_LIBRARY AND ZMQ_INCLUDE_DIRS)
        set(ZMQ_FOUND TRUE)
        message(STATUS "ZeroMQ Found")
        add_definitions( -DBUILD_WITH_ZMQ )
    else(ZMQ_LIBRARY AND ZMQ_INCLUDE_DIRS)
        # Disable  Option if missing dependencies
        set(BUILD_WITH_ZMQ FALSE CACHE BOOL "Build with ZeroMQ" FORCE)
        message(STATUS "ZeroMQ missing. BUILD_WITH_ZMQ option turned OFF")
    endif(ZMQ_LIBRARY AND ZMQ_INCLUDE_DIRS)
endif(BUILD_WITH_ZMQ)

add_library (erebor_wrapper SHARED erebor_wrapper.c)
target_link_libraries (erebor_wrapper LINK_PUBLIC zmq)

add_executable (client client.c)
add_executable (server server.c)

target_link_libraries (client LINK_PUBLIC erebor_wrapper)
target_link_libraries (server LINK_PUBLIC erebor_wrapper)

# Install library
install(TARGETS erebor_wrapper DESTINATION ${CMAKE_INSTALL_PREFIX}/lib/)

# Install library headers
file(GLOB HEADERS *.h)
install(FILES ${HEADERS} DESTINATION ${CMAKE_INSTALL_PREFIX}/include/)
