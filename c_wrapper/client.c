#include <stdio.h>
#include "erebor_wrapper.h"
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>

int main(int argc, char** argv){
    int opt;
    char* in_ipc;
    char* out_ipc = malloc(100);
    char* network;
    char* server;
    char* str_rank = "0";
    int rank =0;
    do
    {
        opt = getopt (argc, argv, "S:R:P:N:");
        switch (opt) {
            case 'S':
                server = optarg;
                break;
            case 'R':
                rank = atoi(optarg);
                str_rank = optarg;
                break;
            case 'P':
                in_ipc = optarg;
                int iout_ipc = atoi(optarg) + 100 + rank;
                sprintf(out_ipc, "%d", iout_ipc);
                break;
            case 'N':
                network = optarg;
                break;
        }
    } while (opt != -1);
    printf("client %d %s %s %s %s\n", rank, in_ipc, out_ipc, network, server);
    erebor* e = malloc(sizeof(erebor));
    int ret = erebor_init_connection(str_rank, network, in_ipc, out_ipc, e);
    if (ret == -1){
        erebor_print_error();
    }else{
        printf("ret %d", ret);
        erebor_send_to(e, "0", "server", "coucou");
        char *dest    = malloc(100);
        char *group   = malloc(99);
        char *message = malloc(100);
        erebor_recv(e, dest, group, message);
        printf("received message %s from %s@%s\n", message, dest, group);
    }
    erebor_close(e);
}
