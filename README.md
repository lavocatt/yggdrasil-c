# Installation

## Requirements

* cmake
* cmake-curses-gui
* pkg-config
* libczmq-dev

### Build C examples

```
cd c_wrapper
ccmake ./
```

Type `c` to configure, make sure `BUILD_WITH_ZMQ` is ON and `ZMQ_DIR` points
where czmq is installed. Then `g` to generate.

```
make
make install
```
